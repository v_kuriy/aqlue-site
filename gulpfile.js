//noinspection JSUnresolvedFunction
const gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    gulpsync = $.sync(gulp),
    browserSync = require('browser-sync'),
    clean = require('gulp-clean'),
    del = require('del'),
    reload = browserSync.reload;

// Cleaning 'dist' folder:
gulp.task('clean', () => {
    log('Cleaning old files');
    return del.sync('dist')
});

// Start server
gulp.task('webserver', () => {
    browserSync({
        server: {
            baseDir: "dist/"
        },
        host: 'localhost',
        port: 8080,
        logPrefix: ''
    });
});

    // Templates, static
// -----------------
gulp.task('templates', () => {
    log('Building templates..');
    return gulp.src('src/templates/*.pug')
        .pipe($.data(function() {
            return require('./src/templates/data/text.json'); // load language file
        }))
        .pipe($.pug({
            doctype: 'html',
            pretty: false
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});

//Styles
//  --------------
gulp.task('styles', () => {
    log('Building application styles..');
    const plugins = [
        autoprefixer({browsers: ['last 1 version']}),
        cssnano()
    ];
    return gulp.src('src/style/landing.scss')
        .pipe($.sass())
        .pipe(postcss(plugins))
        .on('error', handleError)
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});

//Assets
//  --------------
gulp.task('copytoroot', () => {
    log('Copiying root files');
    return gulp.src('src/assets/*.*')
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});
//Fonts
//  --------------
gulp.task('fonts', () => {
    log('Moving font files..');
    return gulp.src('src/fonts/*.*')
        .pipe(gulp.dest('dist/fonts/'))
        .pipe(reload({stream: true}));

});
//Images
//  --------------
gulp.task('images', () => {
    log('Copy images');
    return gulp.src('src/images/*.*')
        .pipe(gulp.dest('dist/images'))
        .pipe(reload({stream: true}));
});

// WATCH
//-------

// Rerun the task when a file changes
gulp.task('watch', () => {
    log('Watching source files..');
    gulp.watch('src/style/**/*', ['styles']);
    gulp.watch('src/templates/**/*', ['templates']);
    gulp.watch('src/images/**/*', ['images']);
});

// Serve files with auto reaload
gulp.task('browsersync', () => {
    log('Starting BrowserSync..');
    browserSync({
        notify: false,
        port: 3010,
        server: {
            baseDir: '..'
        }
    });

});

// Building all sources task:
// Main Tasks
// ----------
gulp.task('assets', [
    'fonts',
    'images',
    'styles',
    'copytoroot',
    'templates'
]);


// default (no minify)
// --------------------
gulp.task('default', gulpsync.sync([
    'clean',
    'assets',
    'webserver',
    'watch'
]));


// Error handler
// ---------------
function handleError(err) {
    log(err.toString());
    this.emit('end');
}

// log to console using
function log(msg) {
    $.util.log($.util.colors.blue(msg));
}