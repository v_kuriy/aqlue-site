To generate static version of aQlue landing page:

1) Check for gulp instalation on your machine, if no, add one with `yarn add gulp` or `npm install gulp`

2) Install dependencies `yarn install` or `npm install`

3) Run gulp building scripts from root directory of the project using `gulp` command

Folder 'dist' will include generated static from templates and style partials. 