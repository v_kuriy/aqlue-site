#!/bin/bash

host="root@qa.aqlue.com"
path="/mnt/persist/www/docroot"
version=`date +%Y%m%d%H%M%S`

echo 'syncing files'
rsync -a dist/* $host:$path/$version

echo 'linking latest'
ssh $host "cd $path; rm -f current; ln -s $version current; exit"